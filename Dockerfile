FROM python:3.10.11

RUN mkdir -p /usr/src/app/

WORKDIR /usr/src/app

COPY app.py /usr/src/app/
COPY storage.py /usr/src/app/

COPY requirements.txt /usr/src/app/

RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "hello.py"]